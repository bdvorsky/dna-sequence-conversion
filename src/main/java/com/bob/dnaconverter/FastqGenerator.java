package com.bob.dnaconverter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FastqGenerator {

    private static final String READ = "@READ_";
    private static final String PLUS_READ = "+READ_";
    private static final int FASTQ_SCORE_ADDITION = 33;
    private static final int BINARY_RADIX = 2;
    private static final String NEWLINE = "\n";


    private static final Map<String, String> BASE_CONVERSION_MAP;

    static {
        BASE_CONVERSION_MAP = new HashMap<>();
        BASE_CONVERSION_MAP.put("00", "A");
        BASE_CONVERSION_MAP.put("01", "C");
        BASE_CONVERSION_MAP.put("10", "G");
        BASE_CONVERSION_MAP.put("11", "T");
    }

    private static FastqGenerator instance;

    private FastqGenerator() {
    }

    public static FastqGenerator getInstance() {
        if (instance == null) {
            instance = new FastqGenerator();
        }
        return instance;
    }

    public void generateFastq(List<String> basesWithScoreList, int readLength, boolean toFile) throws IOException {

        ChunkServingList<String> reads = ChunkServingList.ofSize(basesWithScoreList, readLength);
        OutputPrinter outputPrinter = new OutputPrinter(toFile,readLength);

        for (int i = 0; i < reads.size(); i++) {

            //iterating through reads
            List<String> read = reads.get(i);

            StringBuilder readLettersBuilder = new StringBuilder();
            StringBuilder qualityScoresBuilder = new StringBuilder();

            //iterating through base with score
            for (String baseWithQualityScore : read) {

                //this gets us DNA letter and appends it to the builder
                String base = BASE_CONVERSION_MAP.get(baseWithQualityScore.substring(0, 2));
                readLettersBuilder.append(base);

                //this gets us quality score, converts to ascii and appends to the builder
                int qualityScore = Integer.parseInt(baseWithQualityScore.substring(2, 8), BINARY_RADIX) + FASTQ_SCORE_ADDITION;
                qualityScoresBuilder.append((char) qualityScore);
            }

            String chunkString = READ + (i + 1) + NEWLINE + readLettersBuilder.toString() + NEWLINE + PLUS_READ + (i + 1) + NEWLINE + qualityScoresBuilder.toString();
            outputPrinter.write(chunkString);

        }
        outputPrinter.close();

    }
}
