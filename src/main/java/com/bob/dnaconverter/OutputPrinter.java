package com.bob.dnaconverter;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class OutputPrinter {

    private static final String OUTPUT_FILE_STRING = "output_";
    private static final String FORMAT = ".fastq";

    private BufferedWriter writer;

    public OutputPrinter(boolean toFile, int readLength) throws IOException {
        if(toFile){
                writer = new BufferedWriter(new FileWriter(OUTPUT_FILE_STRING + readLength + FORMAT, true));
        }
    }

    public void write(String stringToWrite) throws IOException {
        if(writer != null){
            writer.write(stringToWrite + "\n");

        } else {
            System.out.println(stringToWrite);
        }
    }

    public void close() throws IOException {
        if(writer != null){
            writer.flush();
            writer.close();
        }
    }

}
