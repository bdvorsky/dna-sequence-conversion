package com.bob.dnaconverter;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;
import java.util.List;

public class SequenceReader {

    private SequenceReader() {
    }

    private static SequenceReader instance;

    public static SequenceReader getInstance() {
        if (instance == null) {
            instance = new SequenceReader();
        }
        return instance;
    }

    public List<String> getBasesWithScoreList(RandomAccessFile inputFile) throws IOException {

        FileChannel inChannel = inputFile.getChannel();
        ByteBuffer buffer = ByteBuffer.allocate(1024);
        List<String> basesWithScoreList = parseSequence(buffer, inChannel);
        inChannel.close();
        return basesWithScoreList;
    }

    private List<String> parseSequence(ByteBuffer buffer, FileChannel inChannel) throws IOException {

        List<String> basesWithScoreList = new ArrayList<>();
        while (inChannel.read(buffer) > 0) {
            buffer.flip();
            for (int i = 0; i < buffer.limit(); i++) {
                byte a = buffer.get();
                //reading byte and converting it to String
                String baseWithScore = String.format("%8s", Integer.toBinaryString(a & 0xFF)).replace(' ', '0');
                basesWithScoreList.add(baseWithScore);
            }
            buffer.clear();
        }
        return basesWithScoreList;
    }

}
