package com.bob.dnaconverter;

import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.List;


public class App {

    public static void main(String[] args) throws IOException {

        //check argumetns length
        if (args.length < 2) {
            throw new IllegalArgumentException("There must be two arguments, path to a input file and read length");
        }

        RandomAccessFile inputFile = new RandomAccessFile(args[0], "r");

        //check if read length isn't bigger than file
        int readLength = Integer.parseInt(args[1]);
        if (readLength > (int) inputFile.length()) {
            throw new IllegalArgumentException("Read length must be smaller than a file size");
        }

        //optional output to file
        boolean toFile = false;
        if(args.length == 3) {
            toFile = Boolean.parseBoolean(args[2]);
        }

        List<String> basesWithScoreList = SequenceReader.getInstance().getBasesWithScoreList(inputFile);
        FastqGenerator.getInstance().generateFastq(basesWithScoreList, readLength, toFile);
    }
}
