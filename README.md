
# README #
    
This repository holds the source code for dna-sequence-converter. 

  

### How do I get set up? ###

* `mvn install` command will build the application as .jar file.
* There is also .jar prepared in the repository under *ready_to_go_application* folder.

### How do get it up and running? ###

* This is a command line application, standard modus operandi can be perform with:

    `java -jar dnaconverter-1.0-SNAPSHOT.jar <path to input file> <read lenght>`
	
	example:
	
	`java -jar dnaconverter-1.0-SNAPSHOT.jar /Users/bohuslavdvorsky/Repositories/private/data/input 15`

* As seen, there are two mandatory attributes - path to input file and read length.

* There is secondary operational mode - 3rd optional parametr, representing boolean - output to file in fastq format. Command signature is following:
`java -jar dnaconverter-1.0-SNAPSHOT.jar <path to input file> <read lenght> <output to file>`

	example:
	
	`java -jar dnaconverter-1.0-SNAPSHOT.jar /Users/bohuslavdvorsky/Repositories/private/data/input 15 true`

* This will generate file in current folder with following signature:

	`output_<read length>.fastq`

  

### Limitations ###

* This works perfectly on small files however with hundreds of megabytes sized file you may experience java.lang.OutOfMemoryError. 

### Possible improvements  ###

* Better exception handling
* Optimizations and rework for large files support